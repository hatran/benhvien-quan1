var datatramyte = [
 {
   "STT": 1,
   "Name": "Trạm y tế Phường Linh Xuân",
   "address": "Phường Linh Xuân, Quận Thủ Đức, Thành phố Hồ Chí Minh",
   "Longtitude": 10.8804079,
   "Latitude": 106.773595
 },
 {
   "STT": 2,
   "Name": "Trạm y tế Phường Bình Chiểu",
   "address": "Phường Bình Chiểu, Quận Thủ Đức, Thành phố Hồ Chí Minh",
   "Longtitude": 10.8830404,
   "Latitude": 106.7264125
 },
 {
   "STT": 3,
   "Name": "Trạm y tế Phường Linh Trung",
   "address": "Phường Linh Trung, Quận Thủ Đức, Thành phố Hồ Chí Minh",
   "Longtitude": 10.8637312,
   "Latitude": 106.7794935
 },
 {
   "STT": 4,
   "Name": "Trạm y tế Phường Tam Bình",
   "address": "Phường Tam Bình, Quận Thủ Đức, Thành phố Hồ Chí Minh",
   "Longtitude": 10.8676413,
   "Latitude": 106.7337841
 },
 {
   "STT": 5,
   "Name": "Trạm y tế Phường Tam Phú",
   "address": "Phường Tam Phú, Quận Thủ Đức, Thành phố Hồ Chí Minh",
   "Longtitude": 10.8551341,
   "Latitude": 106.7382072
 },
 {
   "STT": 6,
   "Name": "Trạm y tế Phường Hiệp Bình Phước",
   "address": "Phường Hiệp Bình Phước, Quận Thủ Đức, Thành phố Hồ Chí Minh",
   "Longtitude": 10.8455467,
   "Latitude": 106.7146184
 },
 {
   "STT": 7,
   "Name": "Trạm y tế Phường Hiệp Bình Chánh",
   "address": "Phường Hiệp Bình Chánh, Quận Thủ Đức, Thành phố Hồ Chí Minh",
   "Longtitude": 10.8339953,
   "Latitude": 106.7264125
 },
 {
   "STT": 8,
   "Name": "Trạm y tế Phường Linh Chiểu",
   "address": "Phường Linh Chiểu, Quận Thủ Đức, Thành phố Hồ Chí Minh",
   "Longtitude": 10.8538209,
   "Latitude": 106.7617984
 },
 {
   "STT": 9,
   "Name": "Trạm y tế Phường Linh Tây",
   "address": "Phường Linh Tây, Quận Thủ Đức, Thành phố Hồ Chí Minh",
   "Longtitude": 10.8560516,
   "Latitude": 106.7535475
 },
 {
   "STT": 10,
   "Name": "Trạm y tế Phường Linh Đông",
   "address": "Phường Linh Đông, Quận Thủ Đức, Thành phố Hồ Chí Minh",
   "Longtitude": 10.843909,
   "Latitude": 106.7441048
 },
 {
   "STT": 11,
   "Name": "Trạm y tế Phường Bình Thọ",
   "address": "Phường Bình Thọ, Quận Thủ Đức, Thành phố Hồ Chí Minh",
   "Longtitude": 10.8467644,
   "Latitude": 106.7662221
 },
 {
   "STT": 12,
   "Name": "Trạm y tế Phường Trường Thọ",
   "address": "Phường Trường Thọ, Quận Thủ Đức, Thành phố Hồ Chí Minh",
   "Longtitude": 10.832358,
   "Latitude": 106.7559004
 },
 {
   "STT": 13,
   "Name": "Trạm y tế Phường Tân Sơn Nhì",
   "address": "Phường Tân Sơn Nhì, Quận Tân Phú, Thành phố Hồ Chí Minh",
   "Longtitude": 10.7997882,
   "Latitude": 106.630604
 },
 {
   "STT": 14,
   "Name": "Trạm y tế Phường Tây Thạnh",
   "address": "Phường Tây Thạnh, Quận Tân Phú, Thành phố Hồ Chí Minh",
   "Longtitude": 10.8122907,
   "Latitude": 106.6261831
 },
 {
   "STT": 15,
   "Name": "Trạm y tế Phường Sơn Kỳ",
   "address": "Phường Sơn Kỳ, Quận Tân Phú, Thành phố Hồ Chí Minh",
   "Longtitude": 10.8033223,
   "Latitude": 106.615868
 },
 {
   "STT": 16,
   "Name": "Trạm y tế Phường Tân Quý",
   "address": "Phường Tân Quý, Quận Tân Phú, Thành phố Hồ Chí Minh",
   "Longtitude": 10.794826,
   "Latitude": 106.6217623
 },
 {
   "STT": 17,
   "Name": "Trạm y tế Phường Tân Thành",
   "address": "Phường Tân Thành, Quận Tân Phú, Thành phố Hồ Chí Minh",
   "Longtitude": 10.7914545,
   "Latitude": 106.6335513
 },
 {
   "STT": 18,
   "Name": "Trạm y tế Phường Phú Thọ Hòa",
   "address": "Phường Phú Thọ Hòa, Quận Tân Phú, Thành phố Hồ Chí Minh",
   "Longtitude": 10.7863307,
   "Latitude": 106.6276567
 },
 {
   "STT": 19,
   "Name": "Trạm y tế Phường Phú Thạnh",
   "address": "Phường Phú Thạnh, Quận Tân Phú, Thành phố Hồ Chí Minh",
   "Longtitude": 10.7785654,
   "Latitude": 106.6317792
 },
 {
   "STT": 20,
   "Name": "Trạm y tế Phường Phú Trung",
   "address": "Phường Phú Trung, Quận Tân Phú, Thành phố Hồ Chí Minh",
   "Longtitude": 10.7773512,
   "Latitude": 106.6423935
 },
 {
   "STT": 21,
   "Name": "Trạm y tế Phường Hòa Thạnh",
   "address": "Phường Hòa Thạnh, Quận Tân Phú, Thành phố Hồ Chí Minh",
   "Longtitude": 10.7783152,
   "Latitude": 106.6372355
 },
 {
   "STT": 22,
   "Name": "Trạm y tế Phường Hiệp Tân",
   "address": "Phường Hiệp Tân, Quận Tân Phú, Thành phố Hồ Chí Minh",
   "Longtitude": 10.7727133,
   "Latitude": 106.6276567
 },
 {
   "STT": 23,
   "Name": "Trạm y tế Phường Tân Thới Hòa",
   "address": "Phường Tân Thới Hòa, Quận Tân Phú, Thành phố Hồ Chí Minh",
   "Longtitude": 10.76294,
   "Latitude": 106.6320777
 },
 {
   "STT": 24,
   "Name": "Trạm y tế Phường 02",
   "address": "Phường 02, Quận Tân Bình, Thành phố Hồ Chí Minh",
   "Longtitude": 10.809839,
   "Latitude": 106.665519
 },
 {
   "STT": 25,
   "Name": "Trạm y tế Phường 04",
   "address": "Phường 04, Quận Tân Bình, Thành phố Hồ Chí Minh",
   "Longtitude": 10.7996982,
   "Latitude": 106.6601129
 },
 {
   "STT": 26,
   "Name": "Trạm y tế Phường 12",
   "address": "Phường 12, Quận Tân Bình, Thành phố Hồ Chí Minh",
   "Longtitude": 10.7931859,
   "Latitude": 106.6500753
 },
 {
   "STT": 27,
   "Name": "Trạm y tế Phường 13",
   "address": "Phường 13, Quận Tân Bình, Thành phố Hồ Chí Minh",
   "Longtitude": 10.8058688,
   "Latitude": 106.6438673
 },
 {
   "STT": 28,
   "Name": "Trạm y tế Phường 01",
   "address": "Phường 01, Quận Tân Bình, Thành phố Hồ Chí Minh",
   "Longtitude": 10.7947121,
   "Latitude": 106.6675593
 },
 {
   "STT": 29,
   "Name": "Trạm y tế Phường 03",
   "address": "Phường 03, Quận Tân Bình, Thành phố Hồ Chí Minh",
   "Longtitude": 10.7935241,
   "Latitude": 106.663193
 },
 {
   "STT": 30,
   "Name": "Trạm y tế Phường 11",
   "address": "Phường 11, Quận Tân Bình, Thành phố Hồ Chí Minh",
   "Longtitude": 10.790694,
   "Latitude": 106.6495246
 },
 {
   "STT": 31,
   "Name": "Trạm y tế Phường 07",
   "address": "Phường 07, Quận Tân Bình, Thành phố Hồ Chí Minh",
   "Longtitude": 10.7881559,
   "Latitude": 106.6563945
 },
 {
   "STT": 32,
   "Name": "Trạm y tế Phường 05",
   "address": "Phường 05, Quận Tân Bình, Thành phố Hồ Chí Minh",
   "Longtitude": 10.7921972,
   "Latitude": 106.6621252
 },
 {
   "STT": 33,
   "Name": "Trạm y tế Phường 10",
   "address": "Phường 10, Quận Tân Bình, Thành phố Hồ Chí Minh",
   "Longtitude": 10.7826359,
   "Latitude": 106.645341
 },
 {
   "STT": 34,
   "Name": "Trạm y tế Phường 06",
   "address": "Phường 06, Quận Tân Bình, Thành phố Hồ Chí Minh",
   "Longtitude": 10.788278,
   "Latitude": 106.6609812
 },
 {
   "STT": 35,
   "Name": "Trạm y tế Phường 08",
   "address": "Phường 08, Quận Tân Bình, Thành phố Hồ Chí Minh",
   "Longtitude": 10.7829523,
   "Latitude": 106.651973
 },
 {
   "STT": 36,
   "Name": "Trạm y tế Phường 09",
   "address": "Phường 09, Quận Tân Bình, Thành phố Hồ Chí Minh",
   "Longtitude": 10.7978921,
   "Latitude": 106.641869
 },
 {
   "STT": 37,
   "Name": "Trạm y tế Phường 14",
   "address": "Phường 14, Quận Tân Bình, Thành phố Hồ Chí Minh",
   "Longtitude": 10.7936922,
   "Latitude": 106.6423935
 },
 {
   "STT": 38,
   "Name": "Trạm y tế Phường 15",
   "address": "Phường 15, Quận Tân Bình, Thành phố Hồ Chí Minh",
   "Longtitude": 10.8225398,
   "Latitude": 106.6379724
 },
 {
   "STT": 39,
   "Name": "Trạm y tế Phường 04",
   "address": "Phường 04, Quận Phú Nhuận, Thành phố Hồ Chí Minh",
   "Longtitude": 10.7986886,
   "Latitude": 106.6888581
 },
 {
   "STT": 40,
   "Name": "Trạm y tế Phường 05",
   "address": "Phường 05, Quận Phú Nhuận, Thành phố Hồ Chí Minh",
   "Longtitude": 10.8037921,
   "Latitude": 106.6865951
 },
 {
   "STT": 41,
   "Name": "Trạm y tế Phường 09",
   "address": "Phường 09, Quận Phú Nhuận, Thành phố Hồ Chí Minh",
   "Longtitude": 10.8023486,
   "Latitude": 106.6843559
 },
 {
   "STT": 42,
   "Name": "Trạm y tế Phường 07",
   "address": "Phường 07, Quận Phú Nhuận, Thành phố Hồ Chí Minh",
   "Longtitude": 10.8019882,
   "Latitude": 106.6895581
 },
 {
   "STT": 43,
   "Name": "Trạm y tế Phường 03",
   "address": "Phường 03, Quận Phú Nhuận, Thành phố Hồ Chí Minh",
   "Longtitude": 10.7950424,
   "Latitude": 106.6760471
 },
 {
   "STT": 44,
   "Name": "Trạm y tế Phường 01",
   "address": "Phường 01, Quận Phú Nhuận, Thành phố Hồ Chí Minh",
   "Longtitude": 10.7991844,
   "Latitude": 106.6813993
 },
 {
   "STT": 45,
   "Name": "Trạm y tế Phường 02",
   "address": "Phường 02, Quận Phú Nhuận, Thành phố Hồ Chí Minh",
   "Longtitude": 10.7929639,
   "Latitude": 106.6858412
 },
 {
   "STT": 46,
   "Name": "Trạm y tế Phường 08",
   "address": "Phường 08, Quận Phú Nhuận, Thành phố Hồ Chí Minh",
   "Longtitude": 10.7980758,
   "Latitude": 106.6740811
 },
 {
   "STT": 47,
   "Name": "Trạm y tế Phường 15",
   "address": "Phường 15, Quận Phú Nhuận, Thành phố Hồ Chí Minh",
   "Longtitude": 10.7950274,
   "Latitude": 106.679977
 },
 {
   "STT": 48,
   "Name": "Trạm y tế Phường 10",
   "address": "Phường 10, Quận Phú Nhuận, Thành phố Hồ Chí Minh",
   "Longtitude": 10.7955148,
   "Latitude": 106.6711332
 },
 {
   "STT": 49,
   "Name": "Trạm y tế Phường 11",
   "address": "Phường 11, Quận Phú Nhuận, Thành phố Hồ Chí Minh",
   "Longtitude": 10.7926289,
   "Latitude": 106.6740811
 },
 {
   "STT": 50,
   "Name": "Trạm y tế Phường 17",
   "address": "Phường 17, Quận Phú Nhuận, Thành phố Hồ Chí Minh",
   "Longtitude": 10.7935032,
   "Latitude": 106.682925
 },
 {
   "STT": 51,
   "Name": "Trạm y tế Phường 14",
   "address": "Phường 14, Quận Phú Nhuận, Thành phố Hồ Chí Minh",
   "Longtitude": 10.791592,
   "Latitude": 106.6681854
 },
 {
   "STT": 52,
   "Name": "Trạm y tế Phường 12",
   "address": "Phường 12, Quận Phú Nhuận, Thành phố Hồ Chí Minh",
   "Longtitude": 10.7923853,
   "Latitude": 106.678503
 },
 {
   "STT": 53,
   "Name": "Trạm y tế Phường 13",
   "address": "Phường 13, Quận Phú Nhuận, Thành phố Hồ Chí Minh",
   "Longtitude": 10.790068,
   "Latitude": 106.6711332
 },
 {
   "STT": 54,
   "Name": "Trạm y tế Phường 15",
   "address": "Phường 15, Quận Gò Vấp, Thành phố Hồ Chí Minh",
   "Longtitude": 10.8514643,
   "Latitude": 106.6678466
 },
 {
   "STT": 55,
   "Name": "Trạm y tế Phường 13",
   "address": "Phường 13, Quận Gò Vấp, Thành phố Hồ Chí Minh",
   "Longtitude": 10.8551871,
   "Latitude": 106.6584938
 },
 {
   "STT": 56,
   "Name": "Trạm y tế Phường 17",
   "address": "Phường 17, Quận Gò Vấp, Thành phố Hồ Chí Minh",
   "Longtitude": 10.8409406,
   "Latitude": 106.6748181
 },
 {
   "STT": 57,
   "Name": "Trạm y tế Phường 6",
   "address": "Phường 6, Quận Gò Vấp, Thành phố Hồ Chí Minh",
   "Longtitude": 10.8384391,
   "Latitude": 106.6816998
 },
 {
   "STT": 58,
   "Name": "Trạm y tế Phường 16",
   "address": "Phường 16, Quận Gò Vấp, Thành phố Hồ Chí Minh",
   "Longtitude": 10.846657,
   "Latitude": 106.6650311
 },
 {
   "STT": 59,
   "Name": "Trạm y tế Phường 12",
   "address": "Phường 12, Quận Gò Vấp, Thành phố Hồ Chí Minh",
   "Longtitude": 10.8347203,
   "Latitude": 106.6394461
 },
 {
   "STT": 60,
   "Name": "Trạm y tế Phường 14",
   "address": "Phường 14, Quận Gò Vấp, Thành phố Hồ Chí Minh",
   "Longtitude": 10.8423134,
   "Latitude": 106.6367
 },
 {
   "STT": 61,
   "Name": "Trạm y tế Phường 10",
   "address": "Phường 10, Quận Gò Vấp, Thành phố Hồ Chí Minh",
   "Longtitude": 10.8329295,
   "Latitude": 106.6718702
 },
 {
   "STT": 62,
   "Name": "Trạm y tế Phường 05",
   "address": "Phường 05, Quận Gò Vấp, Thành phố Hồ Chí Minh",
   "Longtitude": 10.803868,
   "Latitude": 106.684734
 },
 {
   "STT": 63,
   "Name": "Trạm y tế Phường 07",
   "address": "Phường 07, Quận Gò Vấp, Thành phố Hồ Chí Minh",
   "Longtitude": 10.829553,
   "Latitude": 106.683662
 },
 {
   "STT": 64,
   "Name": "Trạm y tế Phường 04",
   "address": "Phường 04, Quận Gò Vấp, Thành phố Hồ Chí Minh",
   "Longtitude": 10.8337815,
   "Latitude": 106.684821
 },
 {
   "STT": 65,
   "Name": "Trạm y tế Phường 01",
   "address": "Phường 01, Quận Gò Vấp, Thành phố Hồ Chí Minh",
   "Longtitude": 10.8173779,
   "Latitude": 106.6888976
 },
 {
   "STT": 66,
   "Name": "Trạm y tế Phường 9",
   "address": "Phường 9, Quận Gò Vấp, Thành phố Hồ Chí Minh",
   "Longtitude": 10.8316621,
   "Latitude": 106.6688132
 },
 {
   "STT": 67,
   "Name": "Trạm y tế Phường 8",
   "address": "Phường 8, Quận Gò Vấp, Thành phố Hồ Chí Minh",
   "Longtitude": 10.8395194,
   "Latitude": 106.6512361
 },
 {
   "STT": 68,
   "Name": "Trạm y tế Phường 11",
   "address": "Phường 11, Quận Gò Vấp, Thành phố Hồ Chí Minh",
   "Longtitude": 10.8390308,
   "Latitude": 106.6600791
 },
 {
   "STT": 69,
   "Name": "Trạm y tế Phường 03",
   "address": "Phường 03, Quận Gò Vấp, Thành phố Hồ Chí Minh",
   "Longtitude": 10.8169609,
   "Latitude": 106.683851
 },
 {
   "STT": 70,
   "Name": "Trạm y tế Phường 13",
   "address": "Phường 13, Quận Bình Thạnh, Thành phố Hồ Chí Minh",
   "Longtitude": 10.8256869,
   "Latitude": 106.7042991
 },
 {
   "STT": 71,
   "Name": "Trạm y tế Phường 11",
   "address": "Phường 11, Quận Bình Thạnh, Thành phố Hồ Chí Minh",
   "Longtitude": 10.8180045,
   "Latitude": 106.6954544
 },
 {
   "STT": 72,
   "Name": "Trạm y tế Phường 27",
   "address": "Phường 27, Quận Bình Thạnh, Thành phố Hồ Chí Minh",
   "Longtitude": 10.8165361,
   "Latitude": 106.7219896
 },
 {
   "STT": 73,
   "Name": "Trạm y tế Phường 26",
   "address": "Phường 26, Quận Bình Thạnh, Thành phố Hồ Chí Minh",
   "Longtitude": 10.8131851,
   "Latitude": 106.7087216
 },
 {
   "STT": 74,
   "Name": "Trạm y tế Phường 12",
   "address": "Phường 12, Quận Bình Thạnh, Thành phố Hồ Chí Minh",
   "Longtitude": 10.8122309,
   "Latitude": 106.7013508
 },
 {
   "STT": 75,
   "Name": "Trạm y tế Phường 25",
   "address": "Phường 25, Quận Bình Thạnh, Thành phố Hồ Chí Minh",
   "Longtitude": 10.8040973,
   "Latitude": 106.7153875
 },
 {
   "STT": 76,
   "Name": "Trạm y tế Phường 05",
   "address": "Phường 05, Quận Bình Thạnh, Thành phố Hồ Chí Minh",
   "Longtitude": 10.8105831,
   "Latitude": 106.7091422
 },
 {
   "STT": 77,
   "Name": "Trạm y tế Phường 07",
   "address": "Phường 07, Quận Bình Thạnh, Thành phố Hồ Chí Minh",
   "Longtitude": 10.8072725,
   "Latitude": 106.6925062
 },
 {
   "STT": 78,
   "Name": "Trạm y tế Phường 24",
   "address": "Phường 24, Quận Bình Thạnh, Thành phố Hồ Chí Minh",
   "Longtitude": 10.8058993,
   "Latitude": 106.7050362
 },
 {
   "STT": 79,
   "Name": "Trạm y tế Phường 06",
   "address": "Phường 06, Quận Bình Thạnh, Thành phố Hồ Chí Minh",
   "Longtitude": 10.8068765,
   "Latitude": 106.687347
 },
 {
   "STT": 80,
   "Name": "Trạm y tế Phường 14",
   "address": "Phường 14, Quận Bình Thạnh, Thành phố Hồ Chí Minh",
   "Longtitude": 10.8049449,
   "Latitude": 106.6976655
 },
 {
   "STT": 81,
   "Name": "Trạm y tế Phường 15",
   "address": "Phường 15, Quận Bình Thạnh, Thành phố Hồ Chí Minh",
   "Longtitude": 10.8004527,
   "Latitude": 106.7050362
 },
 {
   "STT": 82,
   "Name": "Trạm y tế Phường 02",
   "address": "Phường 02, Quận Bình Thạnh, Thành phố Hồ Chí Minh",
   "Longtitude": 10.8031982,
   "Latitude": 106.7198065
 },
 {
   "STT": 83,
   "Name": "Trạm y tế Phường 01",
   "address": "Phường 01, Quận Bình Thạnh, Thành phố Hồ Chí Minh",
   "Longtitude": 10.7981365,
   "Latitude": 106.6976655
 },
 {
   "STT": 84,
   "Name": "Trạm y tế Phường 03",
   "address": "Phường 03, Quận Bình Thạnh, Thành phố Hồ Chí Minh",
   "Longtitude": 10.8025714,
   "Latitude": 106.6956873
 },
 {
   "STT": 85,
   "Name": "Trạm y tế Phường 17",
   "address": "Phường 17, Quận Bình Thạnh, Thành phố Hồ Chí Minh",
   "Longtitude": 10.7962866,
   "Latitude": 106.7065103
 },
 {
   "STT": 86,
   "Name": "Trạm y tế Phường 21",
   "address": "Phường 21, Quận Bình Thạnh, Thành phố Hồ Chí Minh",
   "Longtitude": 10.7957882,
   "Latitude": 106.7117589
 },
 {
   "STT": 87,
   "Name": "Trạm y tế Phường 22",
   "address": "Phường 22, Quận Bình Thạnh, Thành phố Hồ Chí Minh",
   "Longtitude": 10.7921907,
   "Latitude": 106.7190411
 },
 {
   "STT": 88,
   "Name": "Trạm y tế Phường 19",
   "address": "Phường 19, Quận Bình Thạnh, Thành phố Hồ Chí Minh",
   "Longtitude": 10.7899564,
   "Latitude": 106.7101958
 },
 {
   "STT": 89,
   "Name": "Trạm y tế Phường 28",
   "address": "Phường 28, Quận Bình Thạnh, Thành phố Hồ Chí Minh",
   "Longtitude": 10.825006,
   "Latitude": 106.7411559
 },
 {
   "STT": 90,
   "Name": "Trạm y tế Phường Bình Hưng Hòa",
   "address": "Phường Bình Hưng Hòa, Quận Bình Tân, Thành phố Hồ Chí Minh",
   "Longtitude": 10.8026884,
   "Latitude": 106.6026064
 },
 {
   "STT": 91,
   "Name": "Trạm y tế Phường Bình Hưng Hoà A",
   "address": "Phường Bình Hưng Hoà A, Quận Bình Tân, Thành phố Hồ Chí Minh",
   "Longtitude": 10.7850966,
   "Latitude": 106.6069312
 },
 {
   "STT": 92,
   "Name": "Trạm y tế Phường Bình Hưng Hoà B",
   "address": "Phường Bình Hưng Hoà B, Quận Bình Tân, Thành phố Hồ Chí Minh",
   "Longtitude": 10.8033353,
   "Latitude": 106.590819
 },
 {
   "STT": 93,
   "Name": "Trạm y tế Phường Bình Trị Đông",
   "address": "Phường Bình Trị Đông, Quận Bình Tân, Thành phố Hồ Chí Minh",
   "Longtitude": 10.7639086,
   "Latitude": 106.6143944
 },
 {
   "STT": 94,
   "Name": "Trạm y tế Phường Bình Trị Đông A",
   "address": "Phường Bình Trị Đông A, Quận Bình Tân, Thành phố Hồ Chí Minh",
   "Longtitude": 10.7703231,
   "Latitude": 106.5967126
 },
 {
   "STT": 95,
   "Name": "Trạm y tế Phường Bình Trị Đông B",
   "address": "Phường Bình Trị Đông B, Quận Bình Tân, Thành phố Hồ Chí Minh",
   "Longtitude": 10.7478932,
   "Latitude": 106.6085003
 },
 {
   "STT": 96,
   "Name": "Trạm y tế Phường Tân Tạo",
   "address": "Phường Tân Tạo, Quận Bình Tân, Thành phố Hồ Chí Minh",
   "Longtitude": 10.7597516,
   "Latitude": 106.590819
 },
 {
   "STT": 97,
   "Name": "Trạm y tế Phường Tân Tạo A",
   "address": "Phường Tân Tạo A, Quận Bình Tân, Thành phố Hồ Chí Minh",
   "Longtitude": 10.7360575,
   "Latitude": 106.5906249
 },
 {
   "STT": 98,
   "Name": "Trạm y tế Phường  An Lạc",
   "address": "Phường  An Lạc, Quận Bình Tân, Thành phố Hồ Chí Minh",
   "Longtitude": 10.7539749,
   "Latitude": 106.6217623
 },
 {
   "STT": 99,
   "Name": "Trạm y tế Phường An Lạc A",
   "address": "Phường An Lạc A, Quận Bình Tân, Thành phố Hồ Chí Minh",
   "Longtitude": 10.7539749,
   "Latitude": 106.6217623
 },
 {
   "STT": 100,
   "Name": "Trạm y tế Phường Thạnh Xuân",
   "address": "Phường Thạnh Xuân, Quận 12, Thành phố Hồ Chí Minh",
   "Longtitude": 10.8834303,
   "Latitude": 106.6703963
 },
 {
   "STT": 101,
   "Name": "Trạm y tế Phường Thạnh Lộc",
   "address": "Phường Thạnh Lộc, Quận 12, Thành phố Hồ Chí Minh",
   "Longtitude": 10.8712302,
   "Latitude": 106.6859815
 },
 {
   "STT": 102,
   "Name": "Trạm y tế Phường Hiệp Thành",
   "address": "Phường Hiệp Thành, Quận 12, Thành phố Hồ Chí Minh",
   "Longtitude": 10.8825023,
   "Latitude": 106.6379724
 },
 {
   "STT": 103,
   "Name": "Trạm y tế Phường Thới An",
   "address": "Phường Thới An, Quận 12, Thành phố Hồ Chí Minh",
   "Longtitude": 10.8760697,
   "Latitude": 106.6556575
 },
 {
   "STT": 104,
   "Name": "Trạm y tế Phường Tân Chánh Hiệp",
   "address": "Phường Tân Chánh Hiệp, Quận 12, Thành phố Hồ Chí Minh",
   "Longtitude": 10.866797,
   "Latitude": 106.6261831
 },
 {
   "STT": 105,
   "Name": "Trạm y tế Phường An Phú Đông",
   "address": "Phường An Phú Đông, Quận 12, Thành phố Hồ Chí Minh",
   "Longtitude": 10.8596614,
   "Latitude": 106.7057733
 },
 {
   "STT": 106,
   "Name": "Trạm y tế Phường Tân Thới Hiệp",
   "address": "Phường Tân Thới Hiệp, Quận 12, Thành phố Hồ Chí Minh",
   "Longtitude": 10.8603672,
   "Latitude": 106.6438673
 },
 {
   "STT": 107,
   "Name": "Trạm y tế Phường Trung Mỹ Tây",
   "address": "Phường Trung Mỹ Tây, Quận 12, Thành phố Hồ Chí Minh",
   "Longtitude": 10.856544,
   "Latitude": 106.6143944
 },
 {
   "STT": 108,
   "Name": "Trạm y tế Phường Tân Hưng Thuận",
   "address": "Phường Tân Hưng Thuận, Quận 12, Thành phố Hồ Chí Minh",
   "Longtitude": 10.8384209,
   "Latitude": 106.6217623
 },
 {
   "STT": 109,
   "Name": "Trạm y tế Phường Đông Hưng Thuận",
   "address": "Phường Đông Hưng Thuận, Quận 12, Thành phố Hồ Chí Minh",
   "Longtitude": 10.8433839,
   "Latitude": 106.630604
 },
 {
   "STT": 110,
   "Name": "Trạm y tế Phường Tân Thới Nhất",
   "address": "Phường Tân Thới Nhất, Quận 12, Thành phố Hồ Chí Minh",
   "Longtitude": 10.8292885,
   "Latitude": 106.6143944
 },
 {
   "STT": 111,
   "Name": "Trạm y tế Phường 15",
   "address": "Phường 15, Quận 11, Thành phố Hồ Chí Minh",
   "Longtitude": 10.7761932,
   "Latitude": 106.6548706
 },
 {
   "STT": 112,
   "Name": "Trạm y tế Phường 05",
   "address": "Phường 05, Quận 11, Thành phố Hồ Chí Minh",
   "Longtitude": 10.7668578,
   "Latitude": 106.6485491
 },
 {
   "STT": 113,
   "Name": "Trạm y tế Phường 14",
   "address": "Phường 14, Quận 11, Thành phố Hồ Chí Minh",
   "Longtitude": 10.7694985,
   "Latitude": 106.6490254
 },
 {
   "STT": 114,
   "Name": "Trạm y tế Phường 11",
   "address": "Phường 11, Quận 11, Thành phố Hồ Chí Minh",
   "Longtitude": 10.7666947,
   "Latitude": 106.6504992
 },
 {
   "STT": 115,
   "Name": "Trạm y tế Phường 03",
   "address": "Phường 03, Quận 11, Thành phố Hồ Chí Minh",
   "Longtitude": 10.7630802,
   "Latitude": 106.641751
 },
 {
   "STT": 116,
   "Name": "Trạm y tế Phường 10",
   "address": "Phường 10, Quận 11, Thành phố Hồ Chí Minh",
   "Longtitude": 10.7630148,
   "Latitude": 106.6431304
 },
 {
   "STT": 117,
   "Name": "Trạm y tế Phường 13",
   "address": "Phường 13, Quận 11, Thành phố Hồ Chí Minh",
   "Longtitude": 10.7638102,
   "Latitude": 106.6534468
 },
 {
   "STT": 118,
   "Name": "Trạm y tế Phường 08",
   "address": "Phường 08, Quận 11, Thành phố Hồ Chí Minh",
   "Longtitude": 10.7606898,
   "Latitude": 106.6482886
 },
 {
   "STT": 119,
   "Name": "Trạm y tế Phường 09",
   "address": "Phường 09, Quận 11, Thành phố Hồ Chí Minh",
   "Longtitude": 10.77062,
   "Latitude": 106.6426
 },
 {
   "STT": 120,
   "Name": "Trạm y tế Phường 12",
   "address": "Phường 12, Quận 11, Thành phố Hồ Chí Minh",
   "Longtitude": 10.7611684,
   "Latitude": 106.651973
 },
 {
   "STT": 121,
   "Name": "Trạm y tế Phường 07",
   "address": "Phường 07, Quận 11, Thành phố Hồ Chí Minh",
   "Longtitude": 10.7605453,
   "Latitude": 106.6596036
 },
 {
   "STT": 122,
   "Name": "Trạm y tế Phường 06",
   "address": "Phường 06, Quận 11, Thành phố Hồ Chí Minh",
   "Longtitude": 10.7599654,
   "Latitude": 106.6552891
 },
 {
   "STT": 123,
   "Name": "Trạm y tế Phường 04",
   "address": "Phường 04, Quận 11, Thành phố Hồ Chí Minh",
   "Longtitude": 10.7629739,
   "Latitude": 106.650084
 },
 {
   "STT": 124,
   "Name": "Trạm y tế Phường 01",
   "address": "Phường 01, Quận 11, Thành phố Hồ Chí Minh",
   "Longtitude": 10.7564503,
   "Latitude": 106.6387092
 },
 {
   "STT": 125,
   "Name": "Trạm y tế Phường 02",
   "address": "Phường 02, Quận 11, Thành phố Hồ Chí Minh",
   "Longtitude": 10.7624894,
   "Latitude": 106.6572398
 },
 {
   "STT": 126,
   "Name": "Trạm y tế Phường 16",
   "address": "Phường 16, Quận 11, Thành phố Hồ Chí Minh",
   "Longtitude": 10.7552446,
   "Latitude": 106.6482886
 },
 {
   "STT": 127,
   "Name": "Trạm y tế Phường 15",
   "address": "Phường 15, Quận 10, Thành phố Hồ Chí Minh",
   "Longtitude": 10.7815015,
   "Latitude": 106.6659746
 },
 {
   "STT": 128,
   "Name": "Trạm y tế Phường 13",
   "address": "Phường 13, Quận 10, Thành phố Hồ Chí Minh",
   "Longtitude": 10.7792565,
   "Latitude": 106.6696593
 },
 {
   "STT": 129,
   "Name": "Trạm y tế Phường 14",
   "address": "Phường 14, Quận 10, Thành phố Hồ Chí Minh",
   "Longtitude": 10.7709337,
   "Latitude": 106.6600791
 },
 {
   "STT": 130,
   "Name": "Trạm y tế Phường 12",
   "address": "Phường 12, Quận 10, Thành phố Hồ Chí Minh",
   "Longtitude": 10.7730084,
   "Latitude": 106.6718702
 },
 {
   "STT": 131,
   "Name": "Trạm y tế Phường 11",
   "address": "Phường 11, Quận 10, Thành phố Hồ Chí Minh",
   "Longtitude": 10.7736443,
   "Latitude": 106.6788715
 },
 {
   "STT": 132,
   "Name": "Trạm y tế Phường 10",
   "address": "Phường 10, Quận 10, Thành phố Hồ Chí Minh",
   "Longtitude": 10.7713857,
   "Latitude": 106.6676482
 },
 {
   "STT": 133,
   "Name": "Trạm y tế Phường 09",
   "address": "Phường 09, Quận 10, Thành phố Hồ Chí Minh",
   "Longtitude": 10.7699787,
   "Latitude": 106.6660344
 },
 {
   "STT": 134,
   "Name": "Trạm y tế Phường 01",
   "address": "Phường 01, Quận 10, Thành phố Hồ Chí Minh",
   "Longtitude": 10.7652375,
   "Latitude": 106.677029
 },
 {
   "STT": 135,
   "Name": "Trạm y tế Phường 08",
   "address": "Phường 08, Quận 10, Thành phố Hồ Chí Minh",
   "Longtitude": 10.7645241,
   "Latitude": 106.6652376
 },
 {
   "STT": 136,
   "Name": "Trạm y tế Phường 02",
   "address": "Phường 02, Quận 10, Thành phố Hồ Chí Minh",
   "Longtitude": 10.7672055,
   "Latitude": 106.6744499
 },
 {
   "STT": 137,
   "Name": "Trạm y tế Phường 04",
   "address": "Phường 04, Quận 10, Thành phố Hồ Chí Minh",
   "Longtitude": 10.7733641,
   "Latitude": 106.6668696
 },
 {
   "STT": 138,
   "Name": "Trạm y tế Phường 07",
   "address": "Phường 07, Quận 10, Thành phố Hồ Chí Minh",
   "Longtitude": 10.7602821,
   "Latitude": 106.6619214
 },
 {
   "STT": 139,
   "Name": "Trạm y tế Phường 05",
   "address": "Phường 05, Quận 10, Thành phố Hồ Chí Minh",
   "Longtitude": 10.7637857,
   "Latitude": 106.66739
 },
 {
   "STT": 140,
   "Name": "Trạm y tế Phường 06",
   "address": "Phường 06, Quận 10, Thành phố Hồ Chí Minh",
   "Longtitude": 10.7618825,
   "Latitude": 106.6637637
 },
 {
   "STT": 141,
   "Name": "Trạm y tế Phường 03",
   "address": "Phường 03, Quận 10, Thành phố Hồ Chí Minh",
   "Longtitude": 10.7634625,
   "Latitude": 106.671131
 },
 {
   "STT": 142,
   "Name": "Trạm y tế Phường Long Bình",
   "address": "Phường Long Bình, Quận 9, Thành phố Hồ Chí Minh",
   "Longtitude": 10.8909381,
   "Latitude": 106.8283134
 },
 {
   "STT": 143,
   "Name": "Trạm y tế Phường Long Thạnh Mỹ",
   "address": "Phường Long Thạnh Mỹ, Quận 9, Thành phố Hồ Chí Minh",
   "Longtitude": 10.8421949,
   "Latitude": 106.8237374
 },
 {
   "STT": 144,
   "Name": "Trạm y tế Phường Tân Phú",
   "address": "Phường Tân Phú, Quận 9, Thành phố Hồ Chí Minh",
   "Longtitude": 10.8569656,
   "Latitude": 106.8030892
 },
 {
   "STT": 145,
   "Name": "Trạm y tế Phường Hiệp Phú",
   "address": "Phường Hiệp Phú, Quận 9, Thành phố Hồ Chí Minh",
   "Longtitude": 10.8486667,
   "Latitude": 106.7809682
 },
 {
   "STT": 146,
   "Name": "Trạm y tế Phường Tăng Nhơn Phú A",
   "address": "Phường Tăng Nhơn Phú A, Quận 9, Thành phố Hồ Chí Minh",
   "Longtitude": 10.8409534,
   "Latitude": 106.79719
 },
 {
   "STT": 147,
   "Name": "Trạm y tế Phường Tăng Nhơn Phú B",
   "address": "Phường Tăng Nhơn Phú B, Quận 9, Thành phố Hồ Chí Minh",
   "Longtitude": 10.8307175,
   "Latitude": 106.7853922
 },
 {
   "STT": 148,
   "Name": "Trạm y tế Phường Phước Long B",
   "address": "Phường Phước Long B, Quận 9, Thành phố Hồ Chí Minh",
   "Longtitude": 10.8147076,
   "Latitude": 106.7794935
 },
 {
   "STT": 149,
   "Name": "Trạm y tế Phường Phước Long A",
   "address": "Phường Phước Long A, Quận 9, Thành phố Hồ Chí Minh",
   "Longtitude": 10.8224164,
   "Latitude": 106.763273
 },
 {
   "STT": 150,
   "Name": "Trạm y tế Phường Trường Thạnh",
   "address": "Phường Trường Thạnh, Quận 9, Thành phố Hồ Chí Minh",
   "Longtitude": 10.8117526,
   "Latitude": 106.8325872
 },
 {
   "STT": 151,
   "Name": "Trạm y tế Phường Long Phước",
   "address": "Phường Long Phước, Quận 9, Thành phố Hồ Chí Minh",
   "Longtitude": 10.8075496,
   "Latitude": 106.8591387
 },
 {
   "STT": 152,
   "Name": "Trạm y tế Phường Long Trường",
   "address": "Phường Long Trường, Quận 9, Thành phố Hồ Chí Minh",
   "Longtitude": 10.7986358,
   "Latitude": 106.8237374
 },
 {
   "STT": 153,
   "Name": "Trạm y tế Phường Phước Bình",
   "address": "Phường Phước Bình, Quận 9, Thành phố Hồ Chí Minh",
   "Longtitude": 10.8137558,
   "Latitude": 106.7721204
 },
 {
   "STT": 154,
   "Name": "Trạm y tế Phường Phú Hữu",
   "address": "Phường Phú Hữu, Quận 9, Thành phố Hồ Chí Minh",
   "Longtitude": 10.7890603,
   "Latitude": 106.8001396
 },
 {
   "STT": 155,
   "Name": "Trạm y tế Phường 08",
   "address": "Phường 08, Quận 8, Thành phố Hồ Chí Minh",
   "Longtitude": 10.7324713,
   "Latitude": 106.6396124
 },
 {
   "STT": 156,
   "Name": "Trạm y tế Phường 02",
   "address": "Phường 02, Quận 8, Thành phố Hồ Chí Minh",
   "Longtitude": 10.7453245,
   "Latitude": 106.6867627
 },
 {
   "STT": 157,
   "Name": "Trạm y tế Phường 01",
   "address": "Phường 01, Quận 8, Thành phố Hồ Chí Minh",
   "Longtitude": 10.7454538,
   "Latitude": 106.6902951
 },
 {
   "STT": 158,
   "Name": "Trạm y tế Phường 03",
   "address": "Phường 03, Quận 8, Thành phố Hồ Chí Minh",
   "Longtitude": 10.74094,
   "Latitude": 106.6656804
 },
 {
   "STT": 159,
   "Name": "Trạm y tế Phường 11",
   "address": "Phường 11, Quận 8, Thành phố Hồ Chí Minh",
   "Longtitude": 10.7482708,
   "Latitude": 106.6637637
 },
 {
   "STT": 160,
   "Name": "Trạm y tế Phường 09",
   "address": "Phường 09, Quận 8, Thành phố Hồ Chí Minh",
   "Longtitude": 10.7386556,
   "Latitude": 106.674788
 },
 {
   "STT": 161,
   "Name": "Trạm y tế Phường 10",
   "address": "Phường 10, Quận 8, Thành phố Hồ Chí Minh",
   "Longtitude": 10.7455487,
   "Latitude": 106.6637637
 },
 {
   "STT": 162,
   "Name": "Trạm y tế Phường 04",
   "address": "Phường 04, Quận 8, Thành phố Hồ Chí Minh",
   "Longtitude": 10.740333,
   "Latitude": 106.6654137
 },
 {
   "STT": 163,
   "Name": "Trạm y tế Phường 13",
   "address": "Phường 13, Quận 8, Thành phố Hồ Chí Minh",
   "Longtitude": 10.7459527,
   "Latitude": 106.6563945
 },
 {
   "STT": 164,
   "Name": "Trạm y tế Phường 12",
   "address": "Phường 12, Quận 8, Thành phố Hồ Chí Minh",
   "Longtitude": 10.7432306,
   "Latitude": 106.6563945
 },
 {
   "STT": 165,
   "Name": "Trạm y tế Phường 05",
   "address": "Phường 05, Quận 8, Thành phố Hồ Chí Minh",
   "Longtitude": 10.7368537,
   "Latitude": 106.666304
 },
 {
   "STT": 166,
   "Name": "Trạm y tế Phường 14",
   "address": "Phường 14, Quận 8, Thành phố Hồ Chí Minh",
   "Longtitude": 10.7396318,
   "Latitude": 106.6475517
 },
 {
   "STT": 167,
   "Name": "Trạm y tế Phường 06",
   "address": "Phường 06, Quận 8, Thành phố Hồ Chí Minh",
   "Longtitude": 10.7334671,
   "Latitude": 106.6482886
 },
 {
   "STT": 168,
   "Name": "Trạm y tế Phường 15",
   "address": "Phường 15, Quận 8, Thành phố Hồ Chí Minh",
   "Longtitude": 10.7257399,
   "Latitude": 106.6332948
 },
 {
   "STT": 169,
   "Name": "Trạm y tế Phường 16",
   "address": "Phường 16, Quận 8, Thành phố Hồ Chí Minh",
   "Longtitude": 10.7251488,
   "Latitude": 106.6261831
 },
 {
   "STT": 170,
   "Name": "Trạm y tế Phường 07",
   "address": "Phường 07, Quận 8, Thành phố Hồ Chí Minh",
   "Longtitude": 10.7046969,
   "Latitude": 106.6210379
 },
 {
   "STT": 171,
   "Name": "Trạm y tế Phường Tân Thuận Đông",
   "address": "Phường Tân Thuận Đông, Quận 7, Thành phố Hồ Chí Minh",
   "Longtitude": 10.7571056,
   "Latitude": 106.7382072
 },
 {
   "STT": 172,
   "Name": "Trạm y tế Phường Tân Thuận Tây",
   "address": "Phường Tân Thuận Tây, Quận 7, Thành phố Hồ Chí Minh",
   "Longtitude": 10.7511954,
   "Latitude": 106.7219896
 },
 {
   "STT": 173,
   "Name": "Trạm y tế Phường Tân Kiểng",
   "address": "Phường Tân Kiểng, Quận 7, Thành phố Hồ Chí Minh",
   "Longtitude": 10.7491228,
   "Latitude": 106.7101958
 },
 {
   "STT": 174,
   "Name": "Trạm y tế Phường Tân Hưng",
   "address": "Phường Tân Hưng, Quận 7, Thành phố Hồ Chí Minh",
   "Longtitude": 10.7430482,
   "Latitude": 106.6969285
 },
 {
   "STT": 175,
   "Name": "Trạm y tế Phường Bình Thuận",
   "address": "Phường Bình Thuận, Quận 7, Thành phố Hồ Chí Minh",
   "Longtitude": 10.7414271,
   "Latitude": 106.7264125
 },
 {
   "STT": 176,
   "Name": "Trạm y tế Phường Tân Quy",
   "address": "Phường Tân Quy, Quận 7, Thành phố Hồ Chí Minh",
   "Longtitude": 10.7409584,
   "Latitude": 106.7101958
 },
 {
   "STT": 177,
   "Name": "Trạm y tế Phường Phú Thuận",
   "address": "Phường Phú Thuận, Quận 7, Thành phố Hồ Chí Minh",
   "Longtitude": 10.7299898,
   "Latitude": 106.7426687
 },
 {
   "STT": 178,
   "Name": "Trạm y tế Phường Tân Phú",
   "address": "Phường Tân Phú, Quận 7, Thành phố Hồ Chí Minh",
   "Longtitude": 10.7251014,
   "Latitude": 106.7264125
 },
 {
   "STT": 179,
   "Name": "Trạm y tế Phường Tân Phong",
   "address": "Phường Tân Phong, Quận 7, Thành phố Hồ Chí Minh",
   "Longtitude": 10.7318388,
   "Latitude": 106.702825
 },
 {
   "STT": 180,
   "Name": "Trạm y tế Phường Phú Mỹ",
   "address": "Phường Phú Mỹ, Quận 7, Thành phố Hồ Chí Minh",
   "Longtitude": 10.7081313,
   "Latitude": 106.7382072
 },
 {
   "STT": 181,
   "Name": "Trạm y tế Phường 14",
   "address": "Phường 14, Quận 6, Thành phố Hồ Chí Minh",
   "Longtitude": 10.7577874,
   "Latitude": 106.6322597
 },
 {
   "STT": 182,
   "Name": "Trạm y tế Phường 13",
   "address": "Phường 13, Quận 6, Thành phố Hồ Chí Minh",
   "Longtitude": 10.7536523,
   "Latitude": 106.6276567
 },
 {
   "STT": 183,
   "Name": "Trạm y tế Phường 09",
   "address": "Phường 09, Quận 6, Thành phố Hồ Chí Minh",
   "Longtitude": 10.7656471,
   "Latitude": 106.6601133
 },
 {
   "STT": 184,
   "Name": "Trạm y tế Phường 06",
   "address": "Phường 06, Quận 6, Thành phố Hồ Chí Minh",
   "Longtitude": 10.749057,
   "Latitude": 106.638627
 },
 {
   "STT": 185,
   "Name": "Trạm y tế Phường 12",
   "address": "Phường 12, Quận 6, Thành phố Hồ Chí Minh",
   "Longtitude": 10.7506069,
   "Latitude": 106.6335513
 },
 {
   "STT": 186,
   "Name": "Trạm y tế Phường 05",
   "address": "Phường 05, Quận 6, Thành phố Hồ Chí Minh",
   "Longtitude": 10.7556741,
   "Latitude": 106.64318
 },
 {
   "STT": 187,
   "Name": "Trạm y tế Phường 11",
   "address": "Phường 11, Quận 6, Thành phố Hồ Chí Minh",
   "Longtitude": 10.745162,
   "Latitude": 106.6335513
 },
 {
   "STT": 188,
   "Name": "Trạm y tế Phường 02",
   "address": "Phường 02, Quận 6, Thành phố Hồ Chí Minh",
   "Longtitude": 10.7482405,
   "Latitude": 106.6350216
 },
 {
   "STT": 189,
   "Name": "Trạm y tế Phường 01",
   "address": "Phường 01, Quận 6, Thành phố Hồ Chí Minh",
   "Longtitude": 10.7464886,
   "Latitude": 106.6533811
 },
 {
   "STT": 190,
   "Name": "Trạm y tế Phường 04",
   "address": "Phường 04, Quận 6, Thành phố Hồ Chí Minh",
   "Longtitude": 10.7398794,
   "Latitude": 106.6430785
 },
 {
   "STT": 191,
   "Name": "Trạm y tế Phường 08",
   "address": "Phường 08, Quận 6, Thành phố Hồ Chí Minh",
   "Longtitude": 10.742838,
   "Latitude": 106.6387092
 },
 {
   "STT": 192,
   "Name": "Trạm y tế Phường 03",
   "address": "Phường 03, Quận 6, Thành phố Hồ Chí Minh",
   "Longtitude": 10.7480929,
   "Latitude": 106.6352362
 },
 {
   "STT": 193,
   "Name": "Trạm y tế Phường 07",
   "address": "Phường 07, Quận 6, Thành phố Hồ Chí Minh",
   "Longtitude": 10.7387547,
   "Latitude": 106.6387092
 },
 {
   "STT": 194,
   "Name": "Trạm y tế Phường 10",
   "address": "Phường 10, Quận 6, Thành phố Hồ Chí Minh",
   "Longtitude": 10.7403696,
   "Latitude": 106.6238525
 },
 {
   "STT": 195,
   "Name": "Trạm y tế Phường 04",
   "address": "Phường 04, Quận 5, Thành phố Hồ Chí Minh",
   "Longtitude": 10.7581844,
   "Latitude": 106.6698619
 },
 {
   "STT": 196,
   "Name": "Trạm y tế Phường 09",
   "address": "Phường 09, Quận 5, Thành phố Hồ Chí Minh",
   "Longtitude": 10.761,
   "Latitude": 106.674
 },
 {
   "STT": 197,
   "Name": "Trạm y tế Phường 03",
   "address": "Phường 03, Quận 5, Thành phố Hồ Chí Minh",
   "Longtitude": 10.7540279,
   "Latitude": 106.6633746
 },
 {
   "STT": 198,
   "Name": "Trạm y tế Phường 12",
   "address": "Phường 12, Quận 5, Thành phố Hồ Chí Minh",
   "Longtitude": 10.7564453,
   "Latitude": 106.6601602
 },
 {
   "STT": 199,
   "Name": "Trạm y tế Phường 02",
   "address": "Phường 02, Quận 5, Thành phố Hồ Chí Minh",
   "Longtitude": 10.7605508,
   "Latitude": 106.6747456
 },
 {
   "STT": 200,
   "Name": "Trạm y tế Phường 08",
   "address": "Phường 08, Quận 5, Thành phố Hồ Chí Minh",
   "Longtitude": 10.7557939,
   "Latitude": 106.6692908
 },
 {
   "STT": 201,
   "Name": "Trạm y tế Phường 15",
   "address": "Phường 15, Quận 5, Thành phố Hồ Chí Minh",
   "Longtitude": 10.7556424,
   "Latitude": 106.6534468
 },
 {
   "STT": 202,
   "Name": "Trạm y tế Phường 07",
   "address": "Phường 07, Quận 5, Thành phố Hồ Chí Minh",
   "Longtitude": 10.7545101,
   "Latitude": 106.6740811
 },
 {
   "STT": 203,
   "Name": "Trạm y tế Phường 01",
   "address": "Phường 01, Quận 5, Thành phố Hồ Chí Minh",
   "Longtitude": 10.7533033,
   "Latitude": 106.683662
 },
 {
   "STT": 204,
   "Name": "Trạm y tế Phường 11",
   "address": "Phường 11, Quận 5, Thành phố Hồ Chí Minh",
   "Longtitude": 10.7537961,
   "Latitude": 106.6622899
 },
 {
   "STT": 205,
   "Name": "Trạm y tế Phường 14",
   "address": "Phường 14, Quận 5, Thành phố Hồ Chí Minh",
   "Longtitude": 10.7528392,
   "Latitude": 106.6549206
 },
 {
   "STT": 206,
   "Name": "Trạm y tế Phường 05",
   "address": "Phường 05, Quận 5, Thành phố Hồ Chí Minh",
   "Longtitude": 10.7535612,
   "Latitude": 106.6740173
 },
 {
   "STT": 207,
   "Name": "Trạm y tế Phường 06",
   "address": "Phường 06, Quận 5, Thành phố Hồ Chí Minh",
   "Longtitude": 10.7506695,
   "Latitude": 106.6696593
 },
 {
   "STT": 208,
   "Name": "Trạm y tế Phường 10",
   "address": "Phường 10, Quận 5, Thành phố Hồ Chí Minh",
   "Longtitude": 10.7510738,
   "Latitude": 106.6622899
 },
 {
   "STT": 209,
   "Name": "Trạm y tế Phường 13",
   "address": "Phường 13, Quận 5, Thành phố Hồ Chí Minh",
   "Longtitude": 10.750036,
   "Latitude": 106.6563945
 },
 {
   "STT": 210,
   "Name": "Trạm y tế Phường 12",
   "address": "Phường 12, Quận 4, Thành phố Hồ Chí Minh",
   "Longtitude": 10.7657788,
   "Latitude": 106.7042991
 },
 {
   "STT": 211,
   "Name": "Trạm y tế Phường 13",
   "address": "Phường 13, Quận 4, Thành phố Hồ Chí Minh",
   "Longtitude": 10.762732,
   "Latitude": 106.7101958
 },
 {
   "STT": 212,
   "Name": "Trạm y tế Phường 09",
   "address": "Phường 09, Quận 4, Thành phố Hồ Chí Minh",
   "Longtitude": 10.759888,
   "Latitude": 106.7094642
 },
 {
   "STT": 213,
   "Name": "Trạm y tế Phường 06",
   "address": "Phường 06, Quận 4, Thành phố Hồ Chí Minh",
   "Longtitude": 10.7598569,
   "Latitude": 106.7006138
 },
 {
   "STT": 214,
   "Name": "Trạm y tế Phường 08",
   "address": "Phường 08, Quận 4, Thành phố Hồ Chí Minh",
   "Longtitude": 10.763091,
   "Latitude": 106.7009979
 },
 {
   "STT": 215,
   "Name": "Trạm y tế Phường 10",
   "address": "Phường 10, Quận 4, Thành phố Hồ Chí Minh",
   "Longtitude": 10.760614,
   "Latitude": 106.7054047
 },
 {
   "STT": 216,
   "Name": "Trạm y tế Phường 05",
   "address": "Phường 05, Quận 4, Thành phố Hồ Chí Minh",
   "Longtitude": 10.768006,
   "Latitude": 106.7077011
 },
 {
   "STT": 217,
   "Name": "Trạm y tế Phường 18",
   "address": "Phường 18, Quận 4, Thành phố Hồ Chí Minh",
   "Longtitude": 10.7596852,
   "Latitude": 106.7160926
 },
 {
   "STT": 218,
   "Name": "Trạm y tế Phường 14",
   "address": "Phường 14, Quận 4, Thành phố Hồ Chí Minh",
   "Longtitude": 10.7580903,
   "Latitude": 106.7079845
 },
 {
   "STT": 219,
   "Name": "Trạm y tế Phường 04",
   "address": "Phường 04, Quận 4, Thành phố Hồ Chí Minh",
   "Longtitude": 10.7580055,
   "Latitude": 106.7013719
 },
 {
   "STT": 220,
   "Name": "Trạm y tế Phường 03",
   "address": "Phường 03, Quận 4, Thành phố Hồ Chí Minh",
   "Longtitude": 10.7670894,
   "Latitude": 106.7065888
 },
 {
   "STT": 221,
   "Name": "Trạm y tế Phường 16",
   "address": "Phường 16, Quận 4, Thành phố Hồ Chí Minh",
   "Longtitude": 10.755125,
   "Latitude": 106.7124071
 },
 {
   "STT": 222,
   "Name": "Trạm y tế Phường 02",
   "address": "Phường 02, Quận 4, Thành phố Hồ Chí Minh",
   "Longtitude": 10.7595296,
   "Latitude": 106.6993197
 },
 {
   "STT": 223,
   "Name": "Trạm y tế Phường 15",
   "address": "Phường 15, Quận 4, Thành phố Hồ Chí Minh",
   "Longtitude": 10.7540075,
   "Latitude": 106.7079845
 },
 {
   "STT": 224,
   "Name": "Trạm y tế Phường 01",
   "address": "Phường 01, Quận 4, Thành phố Hồ Chí Minh",
   "Longtitude": 10.7549521,
   "Latitude": 106.6893856
 },
 {
   "STT": 225,
   "Name": "Trạm y tế Phường 08",
   "address": "Phường 08, Quận 3, Thành phố Hồ Chí Minh",
   "Longtitude": 10.7891746,
   "Latitude": 106.687347
 },
 {
   "STT": 226,
   "Name": "Trạm y tế Phường 07",
   "address": "Phường 07, Quận 3, Thành phố Hồ Chí Minh",
   "Longtitude": 10.7830885,
   "Latitude": 106.68661
 },
 {
   "STT": 227,
   "Name": "Trạm y tế Phường 14",
   "address": "Phường 14, Quận 3, Thành phố Hồ Chí Minh",
   "Longtitude": 10.7895808,
   "Latitude": 106.679977
 },
 {
   "STT": 228,
   "Name": "Trạm y tế Phường 12",
   "address": "Phường 12, Quận 3, Thành phố Hồ Chí Minh",
   "Longtitude": 10.788544,
   "Latitude": 106.6740811
 },
 {
   "STT": 229,
   "Name": "Trạm y tế Phường 11",
   "address": "Phường 11, Quận 3, Thành phố Hồ Chí Minh",
   "Longtitude": 10.7860642,
   "Latitude": 106.6696593
 },
 {
   "STT": 230,
   "Name": "Trạm y tế Phường 13",
   "address": "Phường 13, Quận 3, Thành phố Hồ Chí Minh",
   "Longtitude": 10.7855773,
   "Latitude": 106.678503
 },
 {
   "STT": 231,
   "Name": "Trạm y tế Phường 06",
   "address": "Phường 06, Quận 3, Thành phố Hồ Chí Minh",
   "Longtitude": 10.7834495,
   "Latitude": 106.6936593
 },
 {
   "STT": 232,
   "Name": "Trạm y tế Phường 09",
   "address": "Phường 09, Quận 3, Thành phố Hồ Chí Minh",
   "Longtitude": 10.7838236,
   "Latitude": 106.6829554
 },
 {
   "STT": 233,
   "Name": "Trạm y tế Phường 10",
   "address": "Phường 10, Quận 3, Thành phố Hồ Chí Minh",
   "Longtitude": 10.7812538,
   "Latitude": 106.6766605
 },
 {
   "STT": 234,
   "Name": "Trạm y tế Phường 04",
   "address": "Phường 04, Quận 3, Thành phố Hồ Chí Minh",
   "Longtitude": 10.7815683,
   "Latitude": 106.697448
 },
 {
   "STT": 235,
   "Name": "Trạm y tế Phường 05",
   "address": "Phường 05, Quận 3, Thành phố Hồ Chí Minh",
   "Longtitude": 10.77542,
   "Latitude": 106.690196
 },
 {
   "STT": 236,
   "Name": "Trạm y tế Phường 03",
   "address": "Phường 03, Quận 3, Thành phố Hồ Chí Minh",
   "Longtitude": 10.7759388,
   "Latitude": 106.6913861
 },
 {
   "STT": 237,
   "Name": "Trạm y tế Phường 02",
   "address": "Phường 02, Quận 3, Thành phố Hồ Chí Minh",
   "Longtitude": 10.7693112,
   "Latitude": 106.6845602
 },
 {
   "STT": 238,
   "Name": "Trạm y tế Phường 01",
   "address": "Phường 01, Quận 3, Thành phố Hồ Chí Minh",
   "Longtitude": 10.76796,
   "Latitude": 106.677029
 },
 {
   "STT": 239,
   "Name": "Trạm y tế Phường Thảo Điền",
   "address": "Phường Thảo Điền, Quận 2, Thành phố Hồ Chí Minh",
   "Longtitude": 10.8064331,
   "Latitude": 106.7323097
 },
 {
   "STT": 240,
   "Name": "Trạm y tế Phường An Phú",
   "address": "Phường An Phú, Quận 2, Thành phố Hồ Chí Minh",
   "Longtitude": 10.8019128,
   "Latitude": 106.7647475
 },
 {
   "STT": 241,
   "Name": "Trạm y tế Phường Bình An",
   "address": "Phường Bình An, Quận 2, Thành phố Hồ Chí Minh",
   "Longtitude": 10.7915388,
   "Latitude": 106.7308354
 },
 {
   "STT": 242,
   "Name": "Trạm y tế Phường Bình Trưng Đông",
   "address": "Phường Bình Trưng Đông, Quận 2, Thành phố Hồ Chí Minh",
   "Longtitude": 10.7874838,
   "Latitude": 106.7794935
 },
 {
   "STT": 243,
   "Name": "Trạm y tế Phường Bình Trưng Tây",
   "address": "Phường Bình Trưng Tây, Quận 2, Thành phố Hồ Chí Minh",
   "Longtitude": 10.7844627,
   "Latitude": 106.7603239
 },
 {
   "STT": 244,
   "Name": "Trạm y tế Phường Bình Khánh",
   "address": "Phường Bình Khánh, Quận 2, Thành phố Hồ Chí Minh",
   "Longtitude": 10.7830453,
   "Latitude": 106.7367328
 },
 {
   "STT": 245,
   "Name": "Trạm y tế Phường An Khánh",
   "address": "Phường An Khánh, Quận 2, Thành phố Hồ Chí Minh",
   "Longtitude": 10.7814628,
   "Latitude": 106.7160926
 },
 {
   "STT": 246,
   "Name": "Trạm y tế Phường Cát Lái",
   "address": "Phường Cát Lái, Quận 2, Thành phố Hồ Chí Minh",
   "Longtitude": 10.7708268,
   "Latitude": 106.7853922
 },
 {
   "STT": 247,
   "Name": "Trạm y tế Phường Thạnh Mỹ Lợi",
   "address": "Phường Thạnh Mỹ Lợi, Quận 2, Thành phố Hồ Chí Minh",
   "Longtitude": 10.7583621,
   "Latitude": 106.7647475
 },
 {
   "STT": 248,
   "Name": "Trạm y tế Phường An Lợi Đông",
   "address": "Phường An Lợi Đông, Quận 2, Thành phố Hồ Chí Minh",
   "Longtitude": 10.7631993,
   "Latitude": 106.7264125
 },
 {
   "STT": 249,
   "Name": "Trạm y tế Phường Thủ Thiêm",
   "address": "Phường Thủ Thiêm, Quận 2, Thành phố Hồ Chí Minh",
   "Longtitude": 10.7732956,
   "Latitude": 106.7160926
 },
 {
   "STT": 250,
   "Name": "Trạm y tế Phường Tân Định",
   "address": "Phường Tân Định, Quận 1, Thành phố Hồ Chí Minh",
   "Longtitude": 10.7930968,
   "Latitude": 106.6902951
 },
 {
   "STT": 251,
   "Name": "Trạm y tế Phường Đa Kao",
   "address": "Phường Đa Kao, Quận 1, Thành phố Hồ Chí Minh",
   "Longtitude": 10.7878843,
   "Latitude": 106.6984026
 },
 {
   "STT": 252,
   "Name": "Trạm y tế Phường Bến Nghé",
   "address": "Phường Bến Nghé, Quận 1, Thành phố Hồ Chí Minh",
   "Longtitude": 10.7808334,
   "Latitude": 106.702825
 },
 {
   "STT": 253,
   "Name": "Trạm y tế Phường Bến Thành",
   "address": "Phường Bến Thành, Quận 1, Thành phố Hồ Chí Minh",
   "Longtitude": 10.7735994,
   "Latitude": 106.6944173
 },
 {
   "STT": 254,
   "Name": "Trạm y tế Phường Nguyễn Thái Bình",
   "address": "Phường Nguyễn Thái Bình, Quận 1, Thành phố Hồ Chí Minh",
   "Longtitude": 10.7693846,
   "Latitude": 106.7006138
 },
 {
   "STT": 255,
   "Name": "Trạm y tế Phường Phạm Ngũ Lão",
   "address": "Phường Phạm Ngũ Lão, Quận 1, Thành phố Hồ Chí Minh",
   "Longtitude": 10.7658855,
   "Latitude": 106.6908105
 },
 {
   "STT": 256,
   "Name": "Trạm y tế Phường Cầu Ông Lãnh",
   "address": "Phường Cầu Ông Lãnh, Quận 1, Thành phố Hồ Chí Minh",
   "Longtitude": 10.7655446,
   "Latitude": 106.6961914
 },
 {
   "STT": 257,
   "Name": "Trạm y tế Phường Cô Giang",
   "address": "Phường Cô Giang, Quận 1, Thành phố Hồ Chí Minh",
   "Longtitude": 10.7616235,
   "Latitude": 106.6932433
 },
 {
   "STT": 258,
   "Name": "Trạm y tế Phường Nguyễn Cư Trinh",
   "address": "Phường Nguyễn Cư Trinh, Quận 1, Thành phố Hồ Chí Minh",
   "Longtitude": 10.7640301,
   "Latitude": 106.68661
 },
 {
   "STT": 259,
   "Name": "Trạm y tế Phường Cầu Kho",
   "address": "Phường Cầu Kho, Quận 1, Thành phố Hồ Chí Minh",
   "Longtitude": 10.7577834,
   "Latitude": 106.6888211
 },
 {
   "STT": 260,
   "Name": "Trạm y tế Thị trấn Nhà Bè",
   "address": "Thị trấn Nhà Bè, Huyện Nhà Bè, Thành phố Hồ Chí Minh",
   "Longtitude": 10.6943704,
   "Latitude": 106.7411559
 },
 {
   "STT": 261,
   "Name": "Trạm y tế Xã Phước Kiển",
   "address": "Xã Phước Kiển, Huyện Nhà Bè, Thành phố Hồ Chí Minh",
   "Longtitude": 10.70719,
   "Latitude": 106.7057733
 },
 {
   "STT": 262,
   "Name": "Trạm y tế Xã Phước Lộc",
   "address": "Xã Phước Lộc, Huyện Nhà Bè, Thành phố Hồ Chí Minh",
   "Longtitude": 10.7001584,
   "Latitude": 106.685136
 },
 {
   "STT": 263,
   "Name": "Trạm y tế Xã Nhơn Đức",
   "address": "Xã Nhơn Đức, Huyện Nhà Bè, Thành phố Hồ Chí Minh",
   "Longtitude": 10.6751953,
   "Latitude": 106.6939803
 },
 {
   "STT": 264,
   "Name": "Trạm y tế Xã Phú Xuân",
   "address": "Xã Phú Xuân, Huyện Nhà Bè, Thành phố Hồ Chí Minh",
   "Longtitude": 10.6748507,
   "Latitude": 106.7500025
 },
 {
   "STT": 265,
   "Name": "Trạm y tế Xã Long Thới",
   "address": "Xã Long Thới, Huyện Nhà Bè, Thành phố Hồ Chí Minh",
   "Longtitude": 10.6515117,
   "Latitude": 106.7293611
 },
 {
   "STT": 266,
   "Name": "Trạm y tế Xã Hiệp Phước",
   "address": "Xã Hiệp Phước, Huyện Nhà Bè, Thành phố Hồ Chí Minh",
   "Longtitude": 10.6009892,
   "Latitude": 106.7588494
 },
 {
   "STT": 267,
   "Name": "Trạm y tế Thị trấn Hóc Môn",
   "address": "Thị trấn Hóc Môn, Huyện Hóc Môn, Thành phố Hồ Chí Minh",
   "Longtitude": 10.8863934,
   "Latitude": 106.5922924
 },
 {
   "STT": 268,
   "Name": "Trạm y tế Xã Tân Hiệp",
   "address": "Xã Tân Hiệp, Huyện Hóc Môn, Thành phố Hồ Chí Minh",
   "Longtitude": 10.9084099,
   "Latitude": 106.5912473
 },
 {
   "STT": 269,
   "Name": "Trạm y tế Xã Nhị Bình",
   "address": "Xã Nhị Bình, Huyện Hóc Môn, Thành phố Hồ Chí Minh",
   "Longtitude": 10.9132585,
   "Latitude": 106.6733441
 },
 {
   "STT": 270,
   "Name": "Trạm y tế Xã Đông Thạnh",
   "address": "Xã Đông Thạnh, Huyện Hóc Môn, Thành phố Hồ Chí Minh",
   "Longtitude": 10.9065528,
   "Latitude": 106.6468148
 },
 {
   "STT": 271,
   "Name": "Trạm y tế Xã Tân Thới Nhì",
   "address": "Xã Tân Thới Nhì, Huyện Hóc Môn, Thành phố Hồ Chí Minh",
   "Longtitude": 10.8919577,
   "Latitude": 106.5714148
 },
 {
   "STT": 272,
   "Name": "Trạm y tế Xã Thới Tam Thôn",
   "address": "Xã Thới Tam Thôn, Huyện Hóc Môn, Thành phố Hồ Chí Minh",
   "Longtitude": 10.8976059,
   "Latitude": 106.6114474
 },
 {
   "STT": 273,
   "Name": "Trạm y tế Xã Xuân Thới Sơn",
   "address": "Xã Xuân Thới Sơn, Huyện Hóc Môn, Thành phố Hồ Chí Minh",
   "Longtitude": 10.8790427,
   "Latitude": 106.5525145
 },
 {
   "STT": 274,
   "Name": "Trạm y tế Xã Tân Xuân",
   "address": "Xã Tân Xuân, Huyện Hóc Môn, Thành phố Hồ Chí Minh",
   "Longtitude": 10.8684249,
   "Latitude": 106.5967126
 },
 {
   "STT": 275,
   "Name": "Trạm y tế Xã Xuân Thới Đông",
   "address": "Xã Xuân Thới Đông, Huyện Hóc Môn, Thành phố Hồ Chí Minh",
   "Longtitude": 10.8687479,
   "Latitude": 106.5890718
 },
 {
   "STT": 276,
   "Name": "Trạm y tế Xã Trung Chánh",
   "address": "Xã Trung Chánh, Huyện Hóc Môn, Thành phố Hồ Chí Minh",
   "Longtitude": 10.8691278,
   "Latitude": 106.6139587
 },
 {
   "STT": 277,
   "Name": "Trạm y tế Xã Xuân Thới Thượng",
   "address": "Xã Xuân Thới Thượng, Huyện Hóc Môn, Thành phố Hồ Chí Minh",
   "Longtitude": 10.8565784,
   "Latitude": 106.5642998
 },
 {
   "STT": 278,
   "Name": "Trạm y tế Xã Bà Điểm",
   "address": "Xã Bà Điểm, Huyện Hóc Môn, Thành phố Hồ Chí Minh",
   "Longtitude": 10.8411638,
   "Latitude": 106.5967126
 },
 {
   "STT": 279,
   "Name": "Trạm y tế Thị trấn Củ Chi",
   "address": "Thị trấn Củ Chi, Huyện Củ Chi, Thành phố Hồ Chí Minh",
   "Longtitude": 10.972192,
   "Latitude": 106.4965434
 },
 {
   "STT": 280,
   "Name": "Trạm y tế Xã Phú Mỹ Hưng",
   "address": "Xã Phú Mỹ Hưng, Huyện Củ Chi, Thành phố Hồ Chí Minh",
   "Longtitude": 11.1246502,
   "Latitude": 106.458256
 },
 {
   "STT": 281,
   "Name": "Trạm y tế Xã An Phú",
   "address": "Xã An Phú, Huyện Củ Chi, Thành phố Hồ Chí Minh",
   "Longtitude": 11.1168711,
   "Latitude": 106.4994889
 },
 {
   "STT": 282,
   "Name": "Trạm y tế Xã Trung Lập Thượng",
   "address": "Xã Trung Lập Thượng, Huyện Củ Chi, Thành phố Hồ Chí Minh",
   "Longtitude": 11.0603258,
   "Latitude": 106.4346979
 },
 {
   "STT": 283,
   "Name": "Trạm y tế Xã An Nhơn Tây",
   "address": "Xã An Nhơn Tây, Huyện Củ Chi, Thành phố Hồ Chí Minh",
   "Longtitude": 11.074436,
   "Latitude": 106.4759262
 },
 {
   "STT": 284,
   "Name": "Trạm y tế Xã Nhuận Đức",
   "address": "Xã Nhuận Đức, Huyện Củ Chi, Thành phố Hồ Chí Minh",
   "Longtitude": 11.0461163,
   "Latitude": 106.493598
 },
 {
   "STT": 285,
   "Name": "Trạm y tế Xã Phạm Văn Cội",
   "address": "Xã Phạm Văn Cội, Huyện Củ Chi, Thành phố Hồ Chí Minh",
   "Longtitude": 11.0338732,
   "Latitude": 106.5171626
 },
 {
   "STT": 286,
   "Name": "Trạm y tế Xã Phú Hòa Đông",
   "address": "Xã Phú Hòa Đông, Huyện Củ Chi, Thành phố Hồ Chí Minh",
   "Longtitude": 11.0203175,
   "Latitude": 106.5642998
 },
 {
   "STT": 287,
   "Name": "Trạm y tế Xã Trung Lập Hạ",
   "address": "Xã Trung Lập Hạ, Huyện Củ Chi, Thành phố Hồ Chí Minh",
   "Longtitude": 11.026217,
   "Latitude": 106.458256
 },
 {
   "STT": 288,
   "Name": "Trạm y tế Xã Trung An",
   "address": "Xã Trung An, Huyện Củ Chi, Thành phố Hồ Chí Minh",
   "Longtitude": 11.0048859,
   "Latitude": 106.5893129
 },
 {
   "STT": 289,
   "Name": "Trạm y tế Xã Phước Thạnh",
   "address": "Xã Phước Thạnh, Huyện Củ Chi, Thành phố Hồ Chí Minh",
   "Longtitude": 11.011451,
   "Latitude": 106.4288088
 },
 {
   "STT": 290,
   "Name": "Trạm y tế Xã Phước Hiệp",
   "address": "Xã Phước Hiệp, Huyện Củ Chi, Thành phố Hồ Chí Minh",
   "Longtitude": 10.9831531,
   "Latitude": 106.4464766
 },
 {
   "STT": 291,
   "Name": "Trạm y tế Xã Tân An Hội",
   "address": "Xã Tân An Hội, Huyện Củ Chi, Thành phố Hồ Chí Minh",
   "Longtitude": 10.9593528,
   "Latitude": 106.4818167
 },
 {
   "STT": 292,
   "Name": "Trạm y tế Xã Phước Vĩnh An",
   "address": "Xã Phước Vĩnh An, Huyện Củ Chi, Thành phố Hồ Chí Minh",
   "Longtitude": 10.984377,
   "Latitude": 106.5230542
 },
 {
   "STT": 293,
   "Name": "Trạm y tế Xã Thái Mỹ",
   "address": "Xã Thái Mỹ, Huyện Củ Chi, Thành phố Hồ Chí Minh",
   "Longtitude": 10.990917,
   "Latitude": 106.409935
 },
 {
   "STT": 294,
   "Name": "Trạm y tế Xã Tân Thạnh Tây",
   "address": "Xã Tân Thạnh Tây, Huyện Củ Chi, Thành phố Hồ Chí Minh",
   "Longtitude": 10.9875463,
   "Latitude": 106.5642998
 },
 {
   "STT": 295,
   "Name": "Trạm y tế Xã Hòa Phú",
   "address": "Xã Hòa Phú, Huyện Củ Chi, Thành phố Hồ Chí Minh",
   "Longtitude": 10.9765654,
   "Latitude": 106.6143944
 },
 {
   "STT": 296,
   "Name": "Trạm y tế Xã Tân Thạnh Đông",
   "address": "Xã Tân Thạnh Đông, Huyện Củ Chi, Thành phố Hồ Chí Minh",
   "Longtitude": 10.9586085,
   "Latitude": 106.5937658
 },
 {
   "STT": 297,
   "Name": "Trạm y tế Xã Bình Mỹ",
   "address": "Xã Bình Mỹ, Huyện Củ Chi, Thành phố Hồ Chí Minh",
   "Longtitude": 10.9399403,
   "Latitude": 106.635025
 },
 {
   "STT": 298,
   "Name": "Trạm y tế Xã Tân Phú Trung",
   "address": "Xã Tân Phú Trung, Huyện Củ Chi, Thành phố Hồ Chí Minh",
   "Longtitude": 10.9389375,
   "Latitude": 106.5411065
 },
 {
   "STT": 299,
   "Name": "Trạm y tế Xã Tân Thông Hội",
   "address": "Xã Tân Thông Hội, Huyện Củ Chi, Thành phố Hồ Chí Minh",
   "Longtitude": 10.9471303,
   "Latitude": 106.50538
 },
 {
   "STT": 300,
   "Name": "Trạm y tế Thị trấn Cần Thạnh",
   "address": "Thị trấn Cần Thạnh, Huyện Cần Giờ, Thành phố Hồ Chí Minh",
   "Longtitude": 10.4155124,
   "Latitude": 106.9731189
 },
 {
   "STT": 301,
   "Name": "Trạm y tế Xã Bình Khánh",
   "address": "Xã Bình Khánh, Huyện Cần Giờ, Thành phố Hồ Chí Minh",
   "Longtitude": 10.637416,
   "Latitude": 106.7883416
 },
 {
   "STT": 302,
   "Name": "Trạm y tế Xã Tam Thôn Hiệp",
   "address": "Xã Tam Thôn Hiệp, Huyện Cần Giờ, Thành phố Hồ Chí Minh",
   "Longtitude": 10.6037491,
   "Latitude": 106.8595671
 },
 {
   "STT": 303,
   "Name": "Trạm y tế Xã An Thới Đông",
   "address": "Xã An Thới Đông, Huyện Cần Giờ, Thành phố Hồ Chí Minh",
   "Longtitude": 10.5549722,
   "Latitude": 106.8060388
 },
 {
   "STT": 304,
   "Name": "Trạm y tế Xã Thạnh An",
   "address": "Xã Thạnh An, Huyện Cần Giờ, Thành phố Hồ Chí Minh",
   "Longtitude": 10.5459417,
   "Latitude": 106.9712791
 },
 {
   "STT": 305,
   "Name": "Trạm y tế Xã Long Hòa",
   "address": "Xã Long Hòa, Huyện Cần Giờ, Thành phố Hồ Chí Minh",
   "Longtitude": 10.4630405,
   "Latitude": 106.9004472
 },
 {
   "STT": 306,
   "Name": "Trạm y tế Xã Lý Nhơn",
   "address": "Xã Lý Nhơn, Huyện Cần Giờ, Thành phố Hồ Chí Minh",
   "Longtitude": 10.468149,
   "Latitude": 106.8060388
 },
 {
   "STT": 307,
   "Name": "Trạm y tế Thị trấn Tân Túc",
   "address": "Thị trấn Tân Túc, Huyện Bình Chánh, Thành phố Hồ Chí Minh",
   "Longtitude": 10.6844901,
   "Latitude": 106.5731392
 },
 {
   "STT": 308,
   "Name": "Trạm y tế Xã Phạm Văn Hai",
   "address": "Xã Phạm Văn Hai, Huyện Bình Chánh, Thành phố Hồ Chí Minh",
   "Longtitude": 10.8148999,
   "Latitude": 106.528946
 },
 {
   "STT": 309,
   "Name": "Trạm y tế Xã Vĩnh Lộc A",
   "address": "Xã Vĩnh Lộc A, Huyện Bình Chánh, Thành phố Hồ Chí Minh",
   "Longtitude": 10.8238657,
   "Latitude": 106.5642998
 },
 {
   "STT": 310,
   "Name": "Trạm y tế Xã Vĩnh Lộc B",
   "address": "Xã Vĩnh Lộc B, Huyện Bình Chánh, Thành phố Hồ Chí Minh",
   "Longtitude": 10.7911649,
   "Latitude": 106.5642998
 },
 {
   "STT": 311,
   "Name": "Trạm y tế Xã Bình Lợi",
   "address": "Xã Bình Lợi, Huyện Bình Chánh, Thành phố Hồ Chí Minh",
   "Longtitude": 10.7756348,
   "Latitude": 106.5096239
 },
 {
   "STT": 312,
   "Name": "Trạm y tế Xã Lê Minh Xuân",
   "address": "Xã Lê Minh Xuân, Huyện Bình Chánh, Thành phố Hồ Chí Minh",
   "Longtitude": 10.7661714,
   "Latitude": 106.5230542
 },
 {
   "STT": 313,
   "Name": "Trạm y tế Xã Tân Nhựt",
   "address": "Xã Tân Nhựt, Huyện Bình Chánh, Thành phố Hồ Chí Minh",
   "Longtitude": 10.7155493,
   "Latitude": 106.5525145
 },
 {
   "STT": 314,
   "Name": "Trạm y tế Xã Tân Kiên",
   "address": "Xã Tân Kiên, Huyện Bình Chánh, Thành phố Hồ Chí Minh",
   "Longtitude": 10.716207,
   "Latitude": 106.5848072
 },
 {
   "STT": 315,
   "Name": "Trạm y tế Xã Bình Hưng",
   "address": "Xã Bình Hưng, Huyện Bình Chánh, Thành phố Hồ Chí Minh",
   "Longtitude": 10.7200104,
   "Latitude": 106.6703963
 },
 {
   "STT": 316,
   "Name": "Trạm y tế Xã Phong Phú",
   "address": "Xã Phong Phú, Huyện Bình Chánh, Thành phố Hồ Chí Minh",
   "Longtitude": 10.6995307,
   "Latitude": 106.6468148
 },
 {
   "STT": 317,
   "Name": "Trạm y tế Xã An Phú Tây",
   "address": "Xã An Phú Tây, Huyện Bình Chánh, Thành phố Hồ Chí Minh",
   "Longtitude": 10.6880126,
   "Latitude": 106.6085003
 },
 {
   "STT": 318,
   "Name": "Trạm y tế Xã Hưng Long",
   "address": "Xã Hưng Long, Huyện Bình Chánh, Thành phố Hồ Chí Minh",
   "Longtitude": 10.6681681,
   "Latitude": 106.6232359
 },
 {
   "STT": 319,
   "Name": "Trạm y tế Xã Đa Phước",
   "address": "Xã Đa Phước, Huyện Bình Chánh, Thành phố Hồ Chí Minh",
   "Longtitude": 10.666245,
   "Latitude": 106.6586052
 },
 {
   "STT": 320,
   "Name": "Trạm y tế Xã Tân Quý Tây",
   "address": "Xã Tân Quý Tây, Huyện Bình Chánh, Thành phố Hồ Chí Minh",
   "Longtitude": 10.666887,
   "Latitude": 106.5967126
 },
 {
   "STT": 321,
   "Name": "Trạm y tế Xã Bình Chánh",
   "address": "Xã Bình Chánh, Huyện Bình Chánh, Thành phố Hồ Chí Minh",
   "Longtitude": 10.6630417,
   "Latitude": 106.5672462
 },
 {
   "STT": 322,
   "Name": "Trạm y tế Xã Quy Đức",
   "address": "Xã Quy Đức, Huyện Bình Chánh, Thành phố Hồ Chí Minh",
   "Longtitude": 10.6425712,
   "Latitude": 106.6438673
 }
];